package com.example.domain.repository

import com.example.domain.model.Comments
import io.reactivex.Single

interface Repository {
    fun getComments(): Single<List<Comments>>

    fun addFavComments(comments: Comments): Single<Unit>

    fun remFavComments(comments: Comments): Single<Unit>

    fun searchComments(comments: Comments): Single<Boolean>

    fun getFavourites(): Single<List<Comments>>
}