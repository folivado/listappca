package com.example.domain.interactor

import com.example.domain.executor.Executor
import com.example.domain.model.Comments
import com.example.domain.repository.Repository


class RemFavCommentsUseCase(private val repository: Repository, executor: Executor) :SingleInteractor<Unit>(executor) {

    fun remFavComments(onSuccess: (Unit) -> Unit,
                    onError: (Throwable) -> Unit,
                    comments: Comments){
        execute(onSuccess, onError, repository.remFavComments(comments))
    }

}
