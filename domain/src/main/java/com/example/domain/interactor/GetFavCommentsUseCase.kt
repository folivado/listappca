package com.example.domain.interactor

import com.example.domain.executor.Executor
import com.example.domain.model.Comments
import com.example.domain.repository.Repository

class GetFavCommentsUseCase(private val repository: Repository, executor: Executor) :SingleInteractor<List<Comments>>(executor) {

        fun getComments(onSuccess: (List<Comments>) -> Unit,
                        onError: (Throwable) -> Unit){
            execute(onSuccess, onError, repository.getFavourites())
        }
}
