package com.example.domain.interactor

import com.example.domain.executor.Executor
import com.example.domain.model.Comments
import com.example.domain.repository.Repository


class SearchCommentsUseCase(private val repository: Repository, executor: Executor) :SingleInteractor<Boolean>(executor) {

    fun searchComments(onSuccess: (Boolean) -> Unit,
                    onError: (Throwable) -> Unit, comments: Comments){
        execute(onSuccess, onError, repository.searchComments(comments))
    }

}
