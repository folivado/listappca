package com.example.domain.interactor

import com.example.domain.executor.Executor
import com.example.domain.model.Comments
import com.example.domain.repository.Repository


class AddFavCommentsUseCase(private val repository: Repository, executor: Executor) :SingleInteractor<Unit>(executor) {

    fun addFavComments(onSuccess: (Unit) -> Unit,
                    onError: (Throwable) -> Unit,
                    comments: Comments){
        execute(onSuccess, onError, repository.addFavComments(comments))
    }

}
