package com.example.listappca.di

import android.content.Context
import com.example.data.createService
import org.kodein.di.Kodein
import com.example.domain.executor.Executor
import com.example.data.dataSource.APIService
import com.example.data.dataSource.CommentsDataSource
import com.example.data.dataSource.database.DataBaseSource
import com.example.data.dataSource.database.RealmDataBaseSource
import com.example.domain.interactor.*
import com.example.domain.repository.Repository
import com.example.listappca.error.AndroidErrorHandler
import com.example.listappca.error.ErrorHandler
import com.example.listappca.executor.RxExecutor
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

/**
 * Modules
 */
fun appModule(context: Context) = Kodein.Module("appModule") {
    bind<Context>() with singleton { context }
    bind<Executor>() with singleton { RxExecutor() }
    bind<ErrorHandler>() with singleton { AndroidErrorHandler(context = context) }
}

val domainModule = Kodein.Module("domainModule") {
    // Add here data dependencies
    bind() from singleton { GetAllCommentsUseCase(repository = instance(), executor = instance()) }

    bind() from singleton { SearchCommentsUseCase(repository = instance(), executor = instance()) }

    bind() from singleton { AddFavCommentsUseCase(repository = instance(), executor = instance()) }

    bind() from singleton { RemFavCommentsUseCase(repository = instance(), executor = instance()) }

    bind() from singleton { GetFavCommentsUseCase(repository = instance(), executor = instance()) }
}

val dataModule = Kodein.Module("dataModule") {
    // Add here data dependencies
    bind<APIService>() with singleton { createService<APIService>(APIService.endpoint) }

    bind<CommentsDataSource>() with singleton { CommentsDataSource(commentsApiService = instance()) }

    bind<Repository>() with singleton { com.example.data.repository.Repository(commentsDataSource = instance(), localDataSource = instance()) }

    bind<DataBaseSource>() with singleton { RealmDataBaseSource() }
}
