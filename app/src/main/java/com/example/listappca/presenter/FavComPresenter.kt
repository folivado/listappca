package com.example.listappca.presenter

import com.example.domain.interactor.GetFavCommentsUseCase
import com.example.domain.model.Comments
import com.example.listappca.error.ErrorHandler


class FavComPresenter(view: FavComPresenter.View,
                      errorHandler: ErrorHandler,
                      private val favCommentsUseCase: GetFavCommentsUseCase) :

        Presenter<FavComPresenter.View>(errorHandler = errorHandler, view = view) {

    override fun initialize() {
        /*
        view.showProgress()
        favCommentsUseCase.getComments(onSuccess = {
            view.showComments(it)
            view.hideProgress() }
                , onError {  })

         */
    }


    override fun resume() {
        // Nothing to do yet

        view.showProgress()
        favCommentsUseCase.getComments(onSuccess = {
            view.showComments(it)
            view.hideProgress()
        }, onError { })
    }


    override fun stop() {
        // Nothing to do yet
        favCommentsUseCase.clear()
    }


    override fun destroy() {
        // Evitamos los leak de memoria del caso de uso
        favCommentsUseCase.clear()
    }


    //fun onCategoryClicked(category: CategoryView) = view.goToCategoryScreen(category.type)
    fun onCommentsClicked(comment: Comments) = view.goToCommentsScreen(comment)


    interface View : Presenter.View {
        fun showComments(comments: List<Comments>)
        fun goToCommentsScreen(comment: Comments)
    }
}
