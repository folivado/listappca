package com.example.listappca.presenter

import com.example.domain.interactor.GetAllCommentsUseCase
import com.example.domain.model.Comments
import com.example.listappca.error.ErrorHandler


class AllComPresenter(view: AllComPresenter.View,
                      errorHandler: ErrorHandler,
                      private val commentsUseCase :GetAllCommentsUseCase) :
                        Presenter<AllComPresenter.View>(errorHandler = errorHandler, view = view){

    override fun initialize() {
        view.showProgress()
        commentsUseCase.getComments(onSuccess = {
            view.showComments(it)
            view.hideProgress() }
                , onError {  })
    }


    override fun resume() {
        // Nothing to do yet
        view.showProgress()
        commentsUseCase.getComments(onSuccess = {
            view.showComments(it)
            view.hideProgress() }
                , onError {  })
    }


    override fun stop() {
        // Nothing to do yet
    }


    override fun destroy() {
        // Evitamos los leak de memoria del caso de uso
        commentsUseCase.clear()
    }




    //fun onCategoryClicked(category: CategoryView) = view.goToCategoryScreen(category.type)
    fun onCommentsClicked(comment: Comments) = view.goToCommentsScreen(comment)


    interface View : Presenter.View {
        fun showComments(comments: List<Comments>)
        fun goToCommentsScreen(comment: Comments)
    }
}
