package com.example.listappca.presenter

import com.example.data.dataSource.database.RealmDataBaseSource
import com.example.data.mapper.toVo
import com.example.domain.interactor.AddFavCommentsUseCase
import com.example.domain.interactor.RemFavCommentsUseCase
import com.example.domain.interactor.SearchCommentsUseCase
import com.example.domain.model.Comments
import com.example.listappca.error.ErrorHandler
import io.reactivex.Single


class DetailsPresenter (view: DetailsPresenter.View,
                        errorHandler: ErrorHandler,
                        private val searchCommentsUseCase: SearchCommentsUseCase,
                        private val addFavCommentsUseCase: AddFavCommentsUseCase,
                        private val remFavCommentsUseCase: RemFavCommentsUseCase) :
                    Presenter<DetailsPresenter.View>(errorHandler = errorHandler, view = view) {


    override fun initialize() {
        //Comprobación de que está el comment ya añadido
        // obtengo el id y según comprobemos se muestra lo correspondiente

        val comments = view.getComment()

        searchCommentsUseCase.searchComments(onSuccess = {
            if (  it  ) {
                view.showButtonDel(true)
                view.showButtonAdd(false)
            }
            else {
                view.showButtonAdd(true)
                view.showButtonDel(false)
            }
        }, onError {  }, comments = comments)

        view.rederComments(comments = comments)
    }



    override fun resume() {
        // Nothing to do yet
    }



    override fun stop() {
        // Nothing to do yet
    }



    override fun destroy() {
        // Nothing to do yet
    }



    fun addToFavorites(){
        addFavCommentsUseCase.addFavComments(onSuccess = {
            view.showButtonDel(true)
            view.showButtonAdd(false)
        }, onError {}, comments = view.getComment())
    }



    fun removeFromFavorites(){
        remFavCommentsUseCase.remFavComments(onSuccess = {
            view.showButtonAdd(true)
            view.showButtonDel(false)
        }, onError {  }, comments = view.getComment())
    }



    interface View : Presenter.View {
        fun showButtonAdd(activate :Boolean)
        fun showButtonDel(activate: Boolean)

        fun getComment(): Comments
        fun rederComments(comments: Comments)
    }
}