package com.example.listappca.presenter

import com.example.listappca.error.ErrorHandler

class MainPresenter(view: MainPresenter.View,
                    errorHandler: ErrorHandler) :
                    Presenter<MainPresenter.View>(errorHandler = errorHandler, view = view) {
    override fun initialize() {
        // nothing to do
    }

    override fun resume() {
        // nothing to do
    }

    override fun stop() {
        // nothing to do
    }

    override fun destroy() {
        // nothing to do
    }

    interface View : Presenter.View {

    }
}