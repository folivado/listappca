package com.example.listapp

import android.view.View
import com.example.domain.model.Comments
import com.example.listappca.R
import com.example.listappca.view.adapter.RootAdapter
import kotlinx.android.synthetic.main.element_view.view.*


class MyAdapter (onItemClickListener: (Comments) -> Unit):
        RootAdapter<Comments>(onItemClickListener = onItemClickListener) {


    class MyViewHolder(elementsView: View): RootViewHolder<Comments>(elementsView){
        override fun bind(model: Comments) {
            itemView.textView.text = model.email
        }
    }

    override fun viewHolder(view: View): RootViewHolder<Comments> = MyViewHolder(view)

    override val itemLayoutId: Int = R.layout.element_view
}