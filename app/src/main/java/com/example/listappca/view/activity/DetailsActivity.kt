package com.example.listappca.view.activity

import android.view.View
import android.widget.Button
import android.widget.ImageButton
import com.bumptech.glide.Glide
import com.example.domain.model.Comments
import com.example.listappca.R
import com.example.listappca.presenter.DetailsPresenter
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_oldmain.*
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

class DetailsActivity : RootActivity<DetailsPresenter.View>(), DetailsPresenter.View{

    override val progress: View by lazy { progressView }

    override val presenter: DetailsPresenter by instance()

    override val layoutResourceId: Int
        get() = R.layout.activity_details

    override val activityModule: Kodein.Module = Kodein.Module {
        bind<DetailsPresenter>() with provider {
            DetailsPresenter(view = this@DetailsActivity,
                    errorHandler = instance(),
                    searchCommentsUseCase = instance(),
                    addFavCommentsUseCase = instance(),
                    remFavCommentsUseCase = instance())
        }
    }



    override fun showButtonAdd(activate: Boolean){
        //val buttonAdd = findViewById<Button>(R.id.buttonAddFav)
        val starAdd = findViewById<ImageButton>(R.id.addFavStarButton)

        if (activate){
            //buttonAdd.visibility = android.view.View.VISIBLE
            starAdd.visibility = View.VISIBLE
        }
        else{
            //buttonAdd.visibility = android.view.View.INVISIBLE
            starAdd.visibility = View.INVISIBLE
        }

        //buttonAdd.isEnabled = activate
        starAdd.isEnabled = activate
    }



    override fun showButtonDel(activate: Boolean){
        //val buttonDel = findViewById<Button>(R.id.buttonRemFav)
        val starDel = findViewById<ImageButton>(R.id.remFavStarButton)

        if (activate) {
            //buttonDel.visibility = android.view.View.VISIBLE
            starDel.visibility = View.VISIBLE
        }
        else{
            //buttonDel.visibility = android.view.View.INVISIBLE
            starDel.visibility = View.INVISIBLE
        }
        //buttonDel.isEnabled = activate
        starDel.isEnabled = activate
    }



    override fun getComment(): Comments {
        return Comments(postId = intent.getIntExtra(POSTID_MESSAGE, -1),
                id = intent.getIntExtra(ID_MESSAGE, -1),
                email = intent.getStringExtra(EMAIL_MESSAGE),
                name = intent.getStringExtra(NAME_MESSAGE),
                body = intent.getStringExtra(BODY_MESSAGE))

    }



    override fun rederComments(comments: Comments){
        textViewEmail.text = comments.email

        textViewName.text = comments.name

        textViewBody.text = comments.body
    }



    override fun initializeUI() {
        Glide.with(this)
                .load("https://www.rover.com/blog/wp-content/uploads/2014/10/sad-dog-600x340.jpg")
                .into(imageViewPic)
    }



    override fun registerListeners() {
        addFavStarButton.setOnClickListener{ presenter.addToFavorites() }

        remFavStarButton.setOnClickListener{ presenter.removeFromFavorites() }
    }
} // DetailsActivity