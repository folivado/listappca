package com.example.listappca.view.fragment

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.model.Comments
import com.example.listapp.MyAdapter
import com.example.listappca.presenter.AllComPresenter
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import com.example.listappca.R
import com.example.listappca.view.activity.*
import kotlinx.android.synthetic.main.fragment_allcom.*
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider


class AllComFragment : RootFragment<AllComPresenter.View>(), AllComPresenter.View {

    override val presenter: AllComPresenter by instance()

    override val layoutResourceId: Int = R.layout.fragment_allcom

    override val fragmentModule: Kodein.Module = Kodein.Module {
        bind<AllComPresenter>() with provider {
            AllComPresenter(view = this@AllComFragment,
                    errorHandler = instance(), commentsUseCase = instance())
        }
    }

    private val adapter = MyAdapter { presenter.onCommentsClicked(it) }



    override fun initializeUI() {
        initializeComments()
    }



    private fun initializeComments() {
        AllComRecyclerView.layoutManager = LinearLayoutManager(this.context)
        AllComRecyclerView.adapter = adapter
    }



    override fun showComments(comments: List<Comments>) {
        adapter.addAll(comments.toMutableList())
    }



    override fun goToCommentsScreen(comment: Comments) {
        // Navegación a detailsActivity
        val intent = Intent(this.context, DetailsActivity::class.java).apply{
            putExtra(POSTID_MESSAGE, comment.postId)
            putExtra(ID_MESSAGE, comment.id)
            putExtra(EMAIL_MESSAGE, comment.email)
            putExtra(NAME_MESSAGE, comment.name)
            putExtra(BODY_MESSAGE, comment.body)
            putExtra(IMAGE_DETAIL, comment.email)
        }
        startActivity(intent)
    }



    override fun registerListeners() {
        //Nothing to do yet
    }

    override fun showProgress() {
        //Nothing to do yet
    }

    override fun hideProgress() {
        //Nothing to do yet
    }
} // MainFragment