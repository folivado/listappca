package com.example.listappca.view.fragment


import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.model.Comments
import com.example.listapp.MyAdapter
import com.example.listappca.R
import com.example.listappca.presenter.AllComPresenter
import com.example.listappca.presenter.FavComPresenter
import com.example.listappca.presenter.Presenter
import com.example.listappca.view.activity.*
import kotlinx.android.synthetic.main.fragment_allcom.*
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider


class FavComFragment : RootFragment<FavComPresenter.View>(), FavComPresenter.View {

    override val presenter: FavComPresenter by instance()

    override val layoutResourceId: Int = R.layout.fragment_allcom

    override val fragmentModule: Kodein.Module = Kodein.Module {
        bind<FavComPresenter>() with provider {
            FavComPresenter(view = this@FavComFragment,
                    errorHandler = instance(), favCommentsUseCase = instance())
        }
    }

    private val adapter = MyAdapter { presenter.onCommentsClicked(it) }



    override fun initializeUI() {
        initializeComments()
    }



    private fun initializeComments() {
        AllComRecyclerView.layoutManager = LinearLayoutManager(this.context)
        AllComRecyclerView.adapter = adapter
    }



    override fun showComments(comments: List<Comments>) {
        adapter.replace(comments.toMutableList())
        //adapter.addAll(comments.toMutableList())
    }



    override fun goToCommentsScreen(comment: Comments) {
        // Navegación a detailsActivity
        val intent = Intent(this.context, DetailsActivity::class.java).apply{
            putExtra(POSTID_MESSAGE, comment.postId)
            putExtra(ID_MESSAGE, comment.id)
            putExtra(EMAIL_MESSAGE, comment.email)
            putExtra(NAME_MESSAGE, comment.name)
            putExtra(BODY_MESSAGE, comment.body)
            putExtra(IMAGE_DETAIL, comment.email)
        }
        startActivity(intent)
    }



    override fun registerListeners() {
        //Nothing to do yet
    }

    override fun showProgress() {
        //Nothing to do yet
    }

    override fun hideProgress() {
        //Nothing to do yet
    }
} // FavComFragment