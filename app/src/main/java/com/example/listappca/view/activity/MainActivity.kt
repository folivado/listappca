package com.example.listappca.view.activity

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.example.listappca.R
import com.example.listappca.presenter.MainPresenter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_oldmain.*
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

class MainActivity : RootActivity<MainPresenter.View>(), MainPresenter.View {

    override val progress: View by lazy { progressView }

    override val presenter: MainPresenter by instance()

    override val layoutResourceId: Int = R.layout.activity_main

    override val activityModule: Kodein.Module = Kodein.Module {
        bind<MainPresenter>() with provider {
            MainPresenter(view = this@MainActivity,
                    errorHandler = instance() )
        }
    }

    override fun initializeUI() {
        val sectionsPagerAdapter = CollectionPagerAdapterVP2(this)

        val viewPager: ViewPager2 = findViewById(R.id.view_pager_main)

        viewPager.adapter = sectionsPagerAdapter

        val tabLayout = findViewById<TabLayout>(R.id.tabs)

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position+1){
                1 -> { tab.text = "ALL COMMENTS" }
                2 -> { tab.text = "FAV COMMENTS" }
            }
        }.attach()
    }

    override fun registerListeners() {
        // TODO("Not yet implemented")
    }
} // MainActivity