package com.example.listappca.view.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.listappca.view.fragment.AllComFragment
import com.example.listappca.view.fragment.FavComFragment


class CollectionPagerAdapterVP2(fragment: FragmentActivity) : FragmentStateAdapter(fragment){

    companion object{
        private val ARG_OBJECT = arrayOf("All", "Fav")
    }

    override fun getItemCount(): Int = 2


    override fun createFragment(position: Int): Fragment {
        // Return a NEW fragment instance in createFragment(int)
        var fragment= Fragment()

        when (position) {
            0 -> fragment = AllComFragment()
            1 -> fragment = FavComFragment()
        }

        fragment.arguments = Bundle().apply {
            // Our object is just an integer :-P
            putInt(ARG_OBJECT[position], position + 1)
        }
        return fragment
    }

/*
    override fun getPageTitle(position: Int): CharSequence {
        return "${ARG_OBJECT[position]} Comments"
    }

 */

}