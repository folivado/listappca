package com.example.listappca.view.activity

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.model.Comments
import com.example.listapp.MyAdapter
import com.example.listappca.R
import com.example.listappca.presenter.AllComPresenter
import kotlinx.android.synthetic.main.activity_oldmain.*
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider



const val POSTID_MESSAGE = "com.example.listapp.POSTID"
const val ID_MESSAGE = "com.example.listapp.ID"
const val EMAIL_MESSAGE = "com.example.listapp.EMAIL"
const val NAME_MESSAGE = "com.example.listapp.NAME"
const val BODY_MESSAGE = "com.example.listapp.BODY"
const val IMAGE_DETAIL = "com.example.listapp.IMAGE"

class OldMainActivity : RootActivity<AllComPresenter.View>(), AllComPresenter.View{

    override val progress: View by lazy { progressView }

    override val presenter: AllComPresenter by instance()

    override val layoutResourceId: Int = R.layout.activity_oldmain

    override val activityModule: Kodein.Module = Kodein.Module {
        bind<AllComPresenter>() with provider {
            AllComPresenter(view = this@OldMainActivity,
                    errorHandler = instance(), commentsUseCase = instance())
        }
    }

    private val adapter = MyAdapter { presenter.onCommentsClicked(it) }



    override fun initializeUI() {
        initializeComments()
    }



    private fun initializeComments() {
        my_recycler_view.layoutManager = LinearLayoutManager(this)
        my_recycler_view.adapter = adapter
    }



    override fun showComments(comments: List<Comments>) {
        adapter.addAll(comments.toMutableList())
    }



    override fun goToCommentsScreen(comment: Comments) {
        // Navegación a detailsActivity
        val intent = Intent(this, DetailsActivity::class.java).apply{
            putExtra(POSTID_MESSAGE, comment.postId)
            putExtra(ID_MESSAGE, comment.id)
            putExtra(EMAIL_MESSAGE, comment.email)
            putExtra(NAME_MESSAGE, comment.name)
            putExtra(BODY_MESSAGE, comment.body)
            putExtra(IMAGE_DETAIL, comment.email)
        }
        startActivity(intent)
    }



    override fun registerListeners() {
        //Nothing to do yet
    }

} //MainActivity