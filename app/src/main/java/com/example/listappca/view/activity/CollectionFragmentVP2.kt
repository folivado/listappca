package com.example.listappca.view.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.listappca.R

class CollectionFragmentVP2 : Fragment() {
    // When requested, this adapter returns a DemoObjectFragment,
    // representing an object in the collection.
    private lateinit var collectionPagerAdapter: CollectionPagerAdapterVP2
    private lateinit var viewPager: ViewPager2

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.activity_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /*
        collectionPagerAdapter = CollectionPagerAdapterVP2(childFragmentManager)
        viewPager = view.findViewById(R.id.view_pager_main)
        viewPager.adapter = this.collectionPagerAdapter

         */
    }

}