package com.example.data.mapper

import com.example.domain.model.Comments
import com.example.data.model.dto.CommentsDto
import com.example.data.model.vo.CommentsVo

fun CommentsDto.toModel(): Comments = Comments(postId = postId, id = id, name = name, email = email, body = body)

fun CommentsVo.toModel(): Comments = Comments(postId = postId, id = id, name = name, email = email, body = body)

fun Comments.toVo(): CommentsVo = CommentsVo(postId = postId, id = id, name = name, email = email, body = body)