package com.example.data.model.vo

import com.example.domain.constants.Constants.Companion.DEFAULT_INT
import com.example.domain.constants.Constants.Companion.EMPTY_STRING
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class CommentsVo (var postId: Int = DEFAULT_INT,
                       @PrimaryKey
                  var id: Int = DEFAULT_INT,
                  var name: String = EMPTY_STRING,
                  var email: String = EMPTY_STRING,
                  var body: String = EMPTY_STRING) :RealmObject()