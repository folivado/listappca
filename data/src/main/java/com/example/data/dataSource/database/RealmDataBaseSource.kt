package com.example.data.dataSource.database

import com.example.data.mapper.toModel
import com.example.data.mapper.toVo
import com.example.data.model.vo.CommentsVo
import com.example.domain.model.Comments
import io.reactivex.Single
import io.realm.Realm
import io.realm.kotlin.where

class RealmDataBaseSource :DataBaseSource{

    override fun addFavComment(comments: Comments): Single<Unit> {
        val realm = Realm.getDefaultInstance()

        realm.use {
            it.executeTransaction{ it.copyToRealmOrUpdate(comments.toVo()) }
        }

        return Single.just(Unit)
    }



    override fun removeFavComment(comments: Comments): Single<Unit> {
        val realm = Realm.getDefaultInstance()
        realm.use {
            it.executeTransaction{ it.where(CommentsVo::class.java).equalTo("id", comments.id).findAll().deleteFirstFromRealm() }
        }

        return Single.just(Unit)
    }



    override fun searchComment(comments: Comments): Single<Boolean> {
        val realm = Realm.getDefaultInstance()
        val commentsVoRead = realm.where(CommentsVo::class.java).equalTo("id", comments.id).findFirst()

        return Single.just(commentsVoRead != null)
    }



    override fun getFavourites(): Single<List<Comments>> {
        val realm = Realm.getDefaultInstance()

        return Single.just(realm.where(CommentsVo::class.java).findAll().toList().map { it.toModel() })
    }
} // RealmDataBaseSource