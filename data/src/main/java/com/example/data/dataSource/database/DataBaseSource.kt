package com.example.data.dataSource.database

import com.example.data.model.vo.CommentsVo
import com.example.domain.model.Comments
import io.reactivex.Single

interface DataBaseSource {
    fun addFavComment(comments: Comments): Single<Unit>

    fun removeFavComment(comments: Comments): Single<Unit>

    fun searchComment(comments: Comments): Single<Boolean>

    fun getFavourites(): Single<List<Comments>>
}