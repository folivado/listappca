package com.example.data.dataSource

import com.example.data.mapper.toModel
import com.example.domain.model.Comments
import io.reactivex.Single

class CommentsDataSource(private val commentsApiService : APIService) {

    fun getComments() :Single<List<Comments>> = commentsApiService.getAllComments().map {it.map { it.toModel() } }

}