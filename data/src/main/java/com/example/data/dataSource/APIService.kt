package com.example.data.dataSource

import com.example.data.model.dto.CommentsDto
import io.reactivex.Single
import retrofit2.http.GET


interface APIService {

    companion object {val endpoint = "https://jsonplaceholder.typicode.com/"}
    @GET ("comments")
    fun getAllComments(): Single<List<CommentsDto>>
}