package com.example.data.repository

import com.example.data.dataSource.CommentsDataSource
import com.example.data.dataSource.database.DataBaseSource
import com.example.domain.model.Comments
import com.example.domain.repository.Repository
import io.reactivex.Single


/**
 * Repository.
 */
class Repository(private val commentsDataSource: CommentsDataSource, private val localDataSource: DataBaseSource) :Repository{


    override fun getComments(): Single<List<Comments>> = commentsDataSource.getComments()

    override fun addFavComments(comments: Comments): Single<Unit> = localDataSource.addFavComment(comments)

    override fun remFavComments(comments: Comments): Single<Unit> = localDataSource.removeFavComment(comments)

    override fun searchComments(comments: Comments): Single<Boolean> = localDataSource.searchComment(comments)

    override fun getFavourites(): Single<List<Comments>> = localDataSource.getFavourites()
}